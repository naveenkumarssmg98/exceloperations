import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;

public class WriteDataToExcel {
    public static void main(String[] args) {

        // Create a new workbook
        Workbook workbook = new XSSFWorkbook();

        // Create a sheet in the workbook
        Sheet sheet = workbook.createSheet("Sheet1");

        // Create a row in the sheet
        Row headerRow = sheet.createRow(0);

        // Create cells in the header row
        Cell cell0 = headerRow.createCell(0);
        cell0.setCellValue("Name");

        Cell cell1 = headerRow.createCell(1);
        cell1.setCellValue("Age");

        Cell cell2 = headerRow.createCell(2);
        cell2.setCellValue("City");

        // Add data to the sheet
        Row dataRow = sheet.createRow(1);
        dataRow.createCell(0).setCellValue("John");
        dataRow.createCell(1).setCellValue(25);
        dataRow.createCell(2).setCellValue("New York");

        // Specify the file path
        String filePath = "E:\\New folder\\New Microsoft Excel Worksheet.xlsx";

        // Write the workbook to a file
        try (
                FileOutputStream fileOut = new FileOutputStream(filePath)) {
            workbook.write(fileOut);
            System.out.println("Excel file written to " + filePath);
        } catch (
                IOException e) {
            e.printStackTrace();
        } finally {
            // Close the workbook to release resources
            try {
                workbook.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    }


