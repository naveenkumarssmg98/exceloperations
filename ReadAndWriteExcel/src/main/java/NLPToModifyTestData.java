import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class NLPToModifyTestData {
    public static FileInputStream getFileInputStreamNLP1(String filePath) throws FileNotFoundException {
        return new FileInputStream(filePath);
    }

    public static Workbook PElogic(FileInputStream fip) throws IOException {
        Workbook workbook = new XSSFWorkbook(fip);
        Sheet sheet = workbook.getSheet("Sheet1");

        Row dataRow = sheet.getRow(0);
        dataRow.getCell(0).setCellValue("EmployeeName");
        dataRow.getCell(1).setCellValue("EmployeeAge");
        dataRow.getCell(2).setCellValue("EmployeeID");

        Row dataRow2 = sheet.getRow(1);
        dataRow2.getCell(0).setCellValue("Rajeshwar");
        dataRow2.getCell(1).setCellValue("36");
        dataRow2.getCell(2).setCellValue("FFE130");
        return workbook;
    }

    public static void NLP2(Workbook outputObject, String filePath) throws IOException {
        try (FileOutputStream fileOut = new FileOutputStream(filePath)) {
            ((Workbook)outputObject).write(fileOut);
            System.out.println("Excel file modified and written to " + filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ((Workbook)outputObject).close();
    }

    public static void main(String[] args) {
        String filePath = "E:\\New folder\\New Microsoft Excel Worksheet.xlsx";

        try {
           FileInputStream data1= NLPToModifyTestData.getFileInputStreamNLP1(filePath);
           Workbook xyz = NLPToModifyTestData.PElogic(data1);
           NLPToModifyTestData.NLP2(xyz, filePath);
            System.out.println("Updated the file and execution is completed");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }




}
