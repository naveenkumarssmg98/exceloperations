import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ModifyExcelData {
    public static void main(String[] args) throws IOException {
        String filePath = "E:\\New folder\\New Microsoft Excel Worksheet.xlsx";
        FileInputStream fileInputStream = new FileInputStream(filePath);



        Workbook workbook = new XSSFWorkbook(fileInputStream);
        Sheet sheet = workbook.getSheet("Sheet1");

        Row dataRow = sheet.getRow(0);
        dataRow.getCell(0).setCellValue("EmployeeName");
        dataRow.getCell(1).setCellValue("EmployeeAge");
        dataRow.getCell(2).setCellValue("EmployeeID");

        Row dataRow2 = sheet.getRow(1);
        dataRow2.getCell(0).setCellValue("Raj Kumar");
        dataRow2.getCell(1).setCellValue("26");
        dataRow2.getCell(2).setCellValue("FFE109");

        try (FileOutputStream fileOut = new FileOutputStream(filePath)) {
            workbook.write(fileOut);
            System.out.println("Excel file modified and written to " + filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        workbook.close();
//modified
    }






}
